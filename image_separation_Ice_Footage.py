
# coding: utf-8

# In[14]:


import cv2
import numpy as np
import sys
import time    
import os


# In[3]:

get_ipython().system('rm -rf ../split_images3')
get_ipython().system('mkdir ../split_images3')
labels = os.listdir("Ice Footage")

skip = 3

for label in labels:
    files = os.listdir("Ice Footage/"+label)
    path = "../split_images3/"+label
    os.mkdir(path)
    for file in files:
        vidcap = cv2.VideoCapture('Ice Footage/'+label+'/'+file)
        success,image = vidcap.read()
        count = 0
        success = True
        path = "../split_images3/"+label+'/'+file.split('.')[0]
        os.mkdir(path)
        print(path)
        while success:
            string = path+"/"+str(count)+".jpg"  
            if count%skip==0:
                cv2.imwrite(string, image)      
            success,image = vidcap.read()
            #print('Read a new frame: ', success)
            count += 1

