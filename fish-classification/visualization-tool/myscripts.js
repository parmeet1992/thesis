var myApp = angular.module('myApp', [ 'ui.bootstrap']);
     myApp.directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
     myApp.service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl){
           var fd = new FormData();
           fd.append('file', file);
           $http.post(uploadUrl, fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           }).success(function(response) {
                console.log(response);
                alert("The prediction is ready.");
            });
          }
         }]);
     myApp.controller('myCtrl', ['$scope', 'fileUpload', function($scope, fileUpload){
        $scope.uploadFile = function(){
           var file = $scope.myFile;
           var uploadUrl = "http://localhost:9000/get_images";
           console.log(file.name)
           fileUpload.uploadFileToUrl(file, uploadUrl)
           $scope.filename = file.name;
        };
        $scope.generate = function(){
           $scope.filepreview = "../"+$scope.filename
           $scope.visualization20 = "../conv_20"+$scope.filename
           $scope.visualization15 = "../conv_15"+$scope.filename
           $scope.visualization10 = "../conv_10"+$scope.filename
           $scope.visualization5 = "../conv_5"+$scope.filename
           console.log($scope.visualization20);
           console.log($scope.visualization15);
           console.log($scope.visualization10);
           console.log($scope.visualization5);
           
        };
        
         $scope.tab = 1;

	 $scope.setTab = function(newTab){
	      $scope.tab = newTab;
	 };

	 $scope.isSet = function(tabNum){
	      return $scope.tab === tabNum;
	 };
                   
     }]);


