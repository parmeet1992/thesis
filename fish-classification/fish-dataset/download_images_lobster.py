from google_images_download import google_images_download   #importing the library

response = google_images_download.googleimagesdownload()   #class instantiation

arguments = {"keywords":"caught lobster","limit":1000,"print_urls":True,
"output_directory":"/home/parmeet/halibut_images","chromedriver":"/usr/local/bin/chromedriver"}   #creating list of arguments
paths = response.download(arguments)   #passing the arguments to the function
print(paths)   #printing absolute paths of the downloaded images
