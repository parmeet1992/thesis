

import numpy as np


startx = 0
starty = 0

start_ang = -90

for i in range(5):
        ang = start_ang + (i*72)
        startx = 42*np.cos(np.deg2rad(ang))
        starty = -42*np.sin(np.deg2rad(ang))
        print(np.round(startx,0),np.round(starty,0))
