
# coding: utf-8

# In[13]:


import keras
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from PIL import Image
import os

from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D,MaxPooling2D,Flatten,Conv1D
from keras.preprocessing import sequence

from sklearn.model_selection import train_test_split


# In[2]:


##loading the image


# In[1]:


from PIL import Image


# In[9]:


labels = []
images = []
for label in os.listdir("split_images"):
    print(label)
    print(len(os.listdir("split_images/"+label)))
    for image in os.listdir("split_images/"+label):
        #img = cv2.imread("split_images/"+label+"/"+image)
        img = Image.open("split_images/"+label+"/"+image)
        img.load()
        labels.append(label)
        images.append(np.array(img))
images = np.array(images)


# In[8]:


np.array(images[0])


# In[10]:


num_labels = len(np.unique(labels))


# In[11]:


images.shape


# In[12]:


from sklearn import preprocessing
le = preprocessing.LabelEncoder()
le_labels = le.fit_transform(labels)
one = preprocessing.OneHotEncoder()
one.fit(le_labels.reshape(-1,1))


# In[11]:


from sklearn.model_selection import KFold
#kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=100)
kf = KFold(n_splits=10)
cvscores = []
for train, test in kf.split(images):
  # create model
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),activation='relu',input_shape=(720, 960, 3)))
    model.add(MaxPooling2D(pool_size=(4, 4), strides=(4, 4)))
    model.add(Conv2D(64, kernel_size=(5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(4, 4),strides=(4, 4)))
    model.add(Conv2D(128, kernel_size=(5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2),strides=(2, 2)))
    model.add(Flatten())
    model.add(Dense(500, activation='relu'))
    model.add(Dense(num_labels,activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    train_labels = le.transform(np.array(labels)[train])
    train_labels = one.transform(train_labels.reshape(-1,1))
    print(train_labels.shape)
    model.fit(images[list(train)],train_labels, epochs=20, batch_size=1, verbose=0)
    # evaluate the model
    test_labels = le.transform(np.array(labels)[test])
    test_labels = one.transform(test_labels.reshape(-1,1))
    scores = model.evaluate(images[list(test)], test_labels, verbose=0)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    cvscores.append(scores[1] * 100)
print("%.2f%% (+/- %.2f%%)" % (numpy.mean(cvscores), numpy.std(cvscores)))


# In[45]:


train_labels = le.transform(np.array(labels)[train])

