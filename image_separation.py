
# coding: utf-8

# In[2]:


import cv2
import numpy as np
import sys
import time    
import os


# In[3]:


label_list = ['Aug 10th Decaying Ice Stopped Luis.m4v','Aug 28th Stopped Ram Ice.m4v','Aug 11th Decaying Ice.m4v','Aug 29th-30th.m4v',
'Aug 14th Decaying.m4v','Discoloured Ice.m4v','Aug 15th Ice Under Pressure.m4v','Sept 5th Helicopter.m4v',
'Aug 16th-17th Helicopter.m4v','Sept 8th Swell.m4v','Aug 20th-26th.m4v']


# In[5]:


#ss = video_to_frames('split_vids/Aug 10th Decaying Ice Stopped Luis.m4v','split_images/')
import cv2
skip = 5
for name in label_list:
    vidcap = cv2.VideoCapture('/storage/split_vids/'+name)
    success,image = vidcap.read()
    count = 0
    success = True
    print(name)
    path = "/storage/split_images/"+name.split('.')[0]
    print(path)
    os.mkdir(path);
    while success:
        string = "/storage/split_images/"+name.split('.')[0]+"/frame"+str(count)+".jpg"  
        print(string)
        if count%5==0:
            cv2.imwrite(string, image)      
        success,image = vidcap.read()
        print('Read a new frame: ', success)
        count += 1


# In[34]:


get_ipython().system('ls /storage/split_vids/')

