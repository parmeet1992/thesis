
# coding: utf-8

# In[2]:


import keras
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from PIL import Image
import os

from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D,MaxPooling2D,Flatten,Conv1D
from keras.preprocessing import sequence

from sklearn.model_selection import train_test_split


import json
import numpy as np
import pandas as pd
from sklearn import preprocessing
import keras
from keras.layers import Input,Dense,Lambda,Dropout
from keras.models import Model
import os
import numpy as np
from keras.preprocessing import image as image_p
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.vgg16 import VGG16
import time
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
import pickle
import matplotlib.image as mpimg
from keras.preprocessing import image
from PIL import Image


# In[3]:


# !pip install --upgrade tensorflow-gpu==1.4.0


# In[4]:


cnn_base = VGG16(input_shape=(224,224,3),include_top=True,weights='imagenet')
# We'll extract features at the final pool layer.
model = Model(
    inputs=cnn_base.input,
    outputs=cnn_base.get_layer('fc2').output
)
def extract_image(image_path,model):
        img = image.load_img(image_path, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)

        features = model.predict(x)
        return features


# In[5]:


cnn_base.summary()


# In[10]:


labels = []
image_features = []

labels_test = []
image_features_test = []

images_path = "../split_images3"

for label in os.listdir(images_path):
    for folder in os.listdir(images_path + "/" +label):
        
        features = []
        labs = []
        
        file_path = images_path+"/"+label+"/"+folder
        listdir = np.array(sorted(os.listdir(file_path),key=lambda x: int(x.split(".")[0])))
        print(label,len(listdir))
        batch_sz = 100
        for file in listdir:
            features.append(np.array(extract_image(file_path+'/'+file,model))[0])
            labs.append(label)
        print("Done processing features")
        X_train,X_test,y_train,y_test = train_test_split(features,labs,shuffle=False,test_size=0.3)
        image_features = image_features + X_train
        labels = labels + y_train

        labels_test = labels_test + y_test
        image_features_test = image_features_test + X_test


# In[11]:


num_labels = len(np.unique(labels))
image_f = np.array(image_features)
image_f_test = np.array(image_features_test)

num_of_train_examples = image_f.shape
print(num_of_train_examples)

from sklearn import preprocessing
le = preprocessing.LabelEncoder()

le.fit(list(labels)+list(labels_test))
le_labels = le.transform(labels)
le_labels_test = le.transform(labels_test)
one = preprocessing.OneHotEncoder()
one.fit(list(le_labels.reshape(-1,1))+list(le_labels_test.reshape(-1,1)))
one_labels = one.transform(le_labels.reshape(-1,1))
one_labels_test = one.transform(le_labels_test.reshape(-1,1))


# In[12]:


image_input = Input(shape=(4096,))
output = Dense(num_labels,activation='softmax')(image_input)
model_dense = Model(image_input, output)
model_dense.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['acc'])
model_dense.save_weights('vgg-simple-model.h5')


# In[ ]:


from sklearn.model_selection import KFold
kf = KFold(n_splits=10,shuffle=True)
validation_cvscores = []
test_cvscores = []
epochs = 1
for train, test in kf.split(image_f):
    model_dense.load_weights('vgg-simple-model.h5')
    hist = model_dense.fit(image_f[train], one_labels[train], epochs=5, verbose=1)
    print(hist.history)
    val_score = model_dense.evaluate(image_f[test], one_labels[test], verbose=0)
    test_score = model_dense.evaluate(image_f_test, one_labels_test, verbose=0)
    validation_cvscores.append(val_score)
    test_cvscores.append(test_score)


# In[15]:


print("Test CV Scores",test_cvscores)
print(np.mean(np.array(validation_cvscores)[:,1]))
print(np.std(np.array(validation_cvscores)[:,1]))
print(np.mean(np.array(test_cvscores)[:,1]))
print(np.std(np.array(test_cvscores)[:,1]))

print("Validation Standard Deviation",np.std(validation_cvscores))
print("Test Standard Deviation",np.std(test_cvscores))

