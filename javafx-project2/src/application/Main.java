package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import java.util.HashMap; // import the HashMap class
import java.util.Optional;


public class Main extends Application
{
	// Declaring the TextArea for Logging
	TextArea logging;
	
	
	HashMap<String, String> bookings = new HashMap<String, String>();
	String reservationName;

	public static void main(String[] args)
	{
		Application.launch(args);
	}

	@Override
	public void start(Stage stage)
	{
		// Create the TextArea
		logging = new TextArea();
		logging.setMaxWidth(300);
		logging.setMaxHeight(150);

		// Create the Labels
		Label roomType = new Label("Select Room Type: ");
		Label month = new Label("Select Month: ");

		// Create the Lists for the ListViews
		ObservableList<String> roomTypeList = FXCollections.<String>observableArrayList("Single", "Double", "Triple", "Quad","Queen","King","Double-Double");
		ObservableList<String> monthList = FXCollections.<String>observableArrayList("January", "February", "March", "April","May","June","July","August","September","October","November","December");

		// Create the ListView for the seasons
		ListView<String> roomTypes = new ListView<>(roomTypeList);
		// Set the Orientation of the ListView
		roomTypes.setOrientation(Orientation.VERTICAL);
		// Set the Size of the ListView
		roomTypes.setPrefSize(120, 100);

		// Update the TextArea when the selected season changes
		roomTypes.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>()
		{
		    public void changed(ObservableValue<? extends String> ov,
		            final String oldvalue, final String newvalue)
		    {
		    	roomtypeChanged(ov, oldvalue, newvalue);
        }});

		// Create the ListView for the months
		ListView<String> months = new ListView<String>();
		// Set the Orientation of the ListView
		months.setOrientation(Orientation.HORIZONTAL);
		// Set the Size of the ListView
		months.setPrefSize(200, 100);
		// Add the items to the ListView
		months.getItems().addAll(monthList);

		months.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>()
		{
		    public void changed(ObservableValue<? extends String> ov,
		            final String oldvalue, final String newvalue)
		    {
		    	monthChanged(ov, oldvalue, newvalue);
        }});

		// Create the Season VBox
		VBox roomTypeSelection = new VBox();
		// Set Spacing to 10 pixels
		roomTypeSelection.setSpacing(10);
		// Add the Label and the List to the VBox
		roomTypeSelection.getChildren().addAll(roomType,roomTypes);
		
		
		// Create the Fruit VBox
		VBox monthSelection = new VBox();
		// Set Spacing to 10 pixels
		monthSelection.setSpacing(10);
		// Add the Label and the List to the VBox
		monthSelection.getChildren().addAll(month,months);

		// Create the GridPane
		GridPane pane = new GridPane();
		// Set the horizontal and vertical gaps between children
		pane.setHgap(10);
		pane.setVgap(5);
		// Add the Season List at position 0
		pane.addColumn(0, roomTypeSelection);
		// Add the Fruit List at position 1
		pane.addColumn(1, monthSelection);
		// Add the TextArea at position 2
		pane.addColumn(2, logging);
        // create a button 
        Button availability = new Button("Check availability"); 
		pane.addColumn(3, availability);
		Button bookRoom = new Button("Book Room"); 
		pane.addColumn(4, bookRoom);
		
		
		EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) 
            { 
            	String selectedMonth = months.getSelectionModel().getSelectedItem();
            	String roomType = roomTypes.getSelectionModel().getSelectedItem();
            	if(selectedMonth==null){
            		logging.setText("Please select month value");
            	}
            	if(roomType==null){
            		logging.setText("Please select room type");
            	}
            	if(bookings.containsKey(selectedMonth+roomType)) {
            		String reservationName = bookings.get(selectedMonth+roomType);
            		logging.setText("Room already booked in the name of:\n"+reservationName+"\n for booked for month:\n"+selectedMonth+" and Room Type: "+roomType);
            	}
            	else {            	
	                
	                TextInputDialog dialog = new TextInputDialog("Bob");
	                
	                dialog.setTitle("Reservation name");
	                dialog.setHeaderText("Enter your name:");
	                dialog.setContentText("Name:");
	         
	                Optional<String> result = dialog.showAndWait();
	         
	                reservationName=result.get();
	                
	                logging.setText("Room booked for month:\n"+selectedMonth+" and Room Type: "+roomType+
	                		"\n in the name of "+reservationName);
	                bookings.put(selectedMonth+roomType, reservationName);

            	}
            } 
        };
        
        EventHandler<ActionEvent> checkEvent = new EventHandler<ActionEvent>() { 
            public void handle(ActionEvent e) 
            { 
            	String selectedMonth = months.getSelectionModel().getSelectedItem();
            	String roomType = roomTypes.getSelectionModel().getSelectedItem();
            	if(selectedMonth==null){
            		logging.setText("Please select month value");
            	}
            	if(roomType==null){
            		logging.setText("Please select room type");
            	}
            	if(bookings.containsKey(selectedMonth+roomType)) {
            		String reservationName = bookings.get(selectedMonth+roomType);
            		logging.setText("Room already booked in the name of :\n"+reservationName+"\n for booked for month:\n"+selectedMonth+" and Room Type: "+roomType);
            	}
            	else {
                logging.setText("Room is available for booking for month :\n"+selectedMonth+" and Room Type: "+roomType);
            	}
                
            } 
        };
        
        
        
        // when button is pressed 
        bookRoom.setOnAction(event); 
        availability.setOnAction(checkEvent);
		
		// Set the Style-properties of the GridPane
		pane.setStyle("-fx-padding: 10;" +
			"-fx-border-style: solid inside;" +
			"-fx-border-width: 2;" +
			"-fx-border-insets: 5;" +
			"-fx-border-radius: 5;" +
			"-fx-border-color: blue;");

		// Create the Scene
		Scene scene = new Scene(pane);
		// Add the Scene to the Stage
		stage.setScene(scene);
		// Set the Title
		stage.setTitle("Hotel reservation availability checker");
		// Display the Stage
		stage.show();
	}

	// Method to display the Season, which has been changed
	public void roomtypeChanged(ObservableValue<? extends String> observable,String oldValue,String newValue)
	{
		String oldText = oldValue == null ? "null" : oldValue.toString();
		String newText = newValue == null ? "null" : newValue.toString();

		logging.setText("Room changed: old = " + oldText + ", new = " + newText + "\n");
	}

	// Method to display the Fruit, which has been changed
	public void monthChanged(ObservableValue<? extends String> observable,String oldValue,String newValue)
	{
		String oldText = oldValue == null ? "null" : oldValue.toString();
		String newText = newValue == null ? "null" : newValue.toString();

		logging.setText("Month changed: old = " + oldText + ", new = " + newText + "\n");
	}


}
